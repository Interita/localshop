<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $supplier->id !!}</p>
</div>

<!-- Title Field -->
<div class="form-group">
    {!! Form::label('title', Lang::get('base.title').':') !!}
    <p>{!! $supplier->title !!}</p>
</div>

<!-- Address Field -->
<div class="form-group">
    {!! Form::label('address', Lang::get('base.address').':') !!}
    <p>{!! $supplier->address !!}</p>
</div>

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', Lang::get('base.name').':') !!}
    <p>{!! $supplier->name !!}</p>
</div>

<!-- Lastname Field -->
<div class="form-group">
    {!! Form::label('lastname', Lang::get('base.lastname').':') !!}
    <p>{!! $supplier->lastname !!}</p>
</div>

<!-- Phone Field -->
<div class="form-group">
    {!! Form::label('phone', Lang::get('base.phone').':') !!}
    <p>{!! $supplier->phone !!}</p>
</div>

<!-- Code Field -->
<div class="form-group">
    {!! Form::label('code', Lang::get('base.code').':') !!}
    <p>{!! $supplier->code !!}</p>
</div>

<!-- Pvm Code Field -->
<div class="form-group">
    {!! Form::label('pvm_code', Lang::get('base.pvm_code').':') !!}
    <p>{!! $supplier->pvm_code !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', Lang::get('base.created_at').':') !!}
    <p>{!! $supplier->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', Lang::get('base.updated_at').':') !!}
    <p>{!! $supplier->updated_at !!}</p>
</div>

