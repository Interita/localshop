@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1 class="pull-left">Trash Products</h1>
    </section>
    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-body">
                    @include('admin.trash.products.table')

                    <div class="clearfix"></div>
                    <a href="{{ route('trash.index') }}" class="btn btn-default">Back</a>
            </div>
        </div>
    </div>
@endsection

