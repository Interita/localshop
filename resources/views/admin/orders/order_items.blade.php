@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1 class="pull-left">
            @lang('base.order_items')
        </h1>

    </section>
    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-body">

                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>@lang('base.order_id')</th>
                            <th>@lang('base.product_id')</th>
                            <th>@lang('base.title')</th>
                            <th>@lang('base.count')</th>
                            <th>@lang('base.price')</th>
                            <th>@lang('base.pvm_group_id')</th>
                            <th>@lang('base.date')</th>                          
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($order_items as $item)
                            <tr>
                                <td>{{ $item->id }}</td>
                                <td>{{ $item->order_id }}</td>
                                <td>{{ $item->product_id }}</td>
                                <td>{{ $item->title }}</td>
                                <td>{{ $item->count }}</td>
                                <td>{{ $item->price }}</td>
                                <td>{{ $item->pvm_group_id }}</td>
                                <td>{{ $item->created_at }}</td>                              
                            </tr>
                        @endforeach
                    </tbody>
                </table>

            </div>
        </div>
    </div>
@endsection