@extends('layouts.app')

@section('scripts')
    <script src="{{ asset('js/orders.js') }}"></script>
@endsection


@section('content')
    <section class="content-header">
        <h1 class="pull-left">@lang('layouts/menu.orders')</h1>

    </section>
    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-body">

                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>@lang('base.title')</th>
                            <th>@lang('base.cash')</th>
                            <th>@lang('base.card')</th>
                            <th>@lang('base.money_change')</th>
                            <th>@lang('base.status')</th>
                            <th>@lang('base.user_id')</th>
                            <th>@lang('base.date')</th>
                            <th colspan="3"></th>                           
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($orders as $order)
                            <tr item="{{ $order->id }}">
                                <td>{{ $order->id }}</td>
                                <td>{{ $order->title }}</td>
                                <td>{{ $order->cash }}</td>
                                <td>{{ $order->card }}</td>
                                <td>{{ $order->money_change }}</td>
                                <td class="status">{{ $order->status }}</td>
                                <td>{{ $order->user_id }}</td>
                                <td>{{ $order->created_at }}</td>
                                <td>                                  
                                    <div class='btn-group'>
                                        <a href="{!! route('order.items', $order->id) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>

                                        <button class='btn btn-default btn-xs null-order'>@lang('base.print')</button>
                                    </div>  
                                </td>                              
                            </tr>
                        @endforeach
                    </tbody>
                </table>

            </div>
        </div>
    </div>
@endsection