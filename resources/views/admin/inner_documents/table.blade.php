<table class="table table-responsive" id="innerDocuments-table">
    <thead>
        <tr>
            <th>@lang('base.file')</th>
            <th>@lang('base.description')</th>
            <th colspan="3"></th>
        </tr>
    </thead>
    <tbody>
    @foreach($innerDocuments as $innerDocument)
        <tr>
            <td>{!! $innerDocument->file !!}</td>
            <td>{!! $innerDocument->description !!}</td>
            <td>
                {!! Form::open(['route' => ['innerDocuments.destroy', $innerDocument->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('innerDocuments.show', [$innerDocument->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('innerDocuments.edit', [$innerDocument->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>