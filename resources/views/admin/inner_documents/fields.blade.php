<!-- File Field -->
<div class="form-group col-sm-6">
    {!! Form::label('file', Lang::get('base.file').':') !!}
    {!! Form::text('file', null, ['class' => 'form-control']) !!}
</div>

<!-- Description Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('description', Lang::get('base.description').':') !!}
    {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit(Lang::get('base.save'), ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('innerDocuments.index') !!}" class="btn btn-default">@lang('base.cancel')</a>
</div>
