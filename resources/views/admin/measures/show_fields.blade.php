<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $measure->id !!}</p>
</div>

<!-- Title Field -->
<div class="form-group">
    {!! Form::label('title', Lang::get('base.title').':') !!}
    <p>{!! $measure->title !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', Lang::get('base.created_at').':') !!}
    <p>{!! $measure->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', Lang::get('base.updated_at').':') !!}
    <p>{!! $measure->updated_at !!}</p>
</div>

