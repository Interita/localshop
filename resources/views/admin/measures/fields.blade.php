<!-- Title Field -->
<div class="form-group col-sm-6">
    {!! Form::label('title', Lang::get('base.title').':') !!}
    {!! Form::text('title', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit(Lang::get('base.save'), ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('measures.index') !!}" class="btn btn-default">@lang('base.cancel')</a>
</div>
