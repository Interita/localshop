<table class="table table-responsive" id="users-table">
    <thead>
        <tr>
            <th></th>
            <th>@lang('base.name')</th>
            <th>@lang('base.lastname')</th>
            <th>@lang('base.email')</th>
            <th>@lang('base.role')</th>
            
            <th colspan="3"></th>
        </tr>
    </thead>
    <tbody>
    @foreach($users as $user)
        <tr>
            <td>
                @if( !empty($user->image) )
                    <img class="small-image" src="{{ asset('storage') }}/{{$user->image}}">
                @endif
            </td>            
            <td>{!! $user->name !!}</td>
            <td>{!! $user->lastname !!}</td>
            <td>{!! $user->email !!}</td>
            <td>
                {{ $user->role->name }}
            </td>

            <td>
                {!! Form::open(['route' => ['users.destroy', $user->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('users.show', [$user->id]) !!}" class='btn btn-default'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('users.edit', [$user->id]) !!}" class='btn btn-default'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>