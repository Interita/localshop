@extends('layouts.app')

@section('scripts')
<script>
    $(document).ready(function(){

        //-------- DATATABLE ------------
        $('#data_table').DataTable( {
            responsive: true,
            "order": [[ 3, "desc" ]],
            "language": {
                "lengthMenu": "Rodyti _MENU_ per puslapį",
                "search": "Ieškoti prekių:",

            }
        });
        //------------------------------
        

        $(document).on('keypress', '#barcode', function(e){   
            if(e.which == 13) {
                location = "products?barcode="+this.value;
            }
        }); 


        $(document).on('click', '#filter-product', function(){
            var category_id = $('#category_id').val();
            var measure_id = $('#measure_id').val();
            var link = '';

            if (category_id){
                link += '&category=' + category_id;
            }

            if (measure_id){
                link += '&measure=' + measure_id;
            }

            if (link){
                link = link.substring(1);
                link = "{{ route('products.index') }}?" + link;                
            }
            else{
               link = "{{ route('products.index') }}"; 
            }

            location = link;
        });

    });      
</script> 
@endsection

@section('content')
    <section class="content-header">
        <h1 class="pull-left">@lang('layouts/menu.products')</h1>
        <h1 class="pull-right">
            <a class="btn btn-primary pull-right" style="margin-top: -10px;margin-bottom: 5px" href="{!! route('products.create') !!}">
                @lang('base.add_new')
            </a>
        </h1>
    </section>
    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-body">

                <div class="product-filters">
                    <div class="row">
                        @include('admin.products.filters')
                    </div>
                   
                </div>

                <div class="shop-datatable">
                    @include('admin.products.datatable')
                </div>

            </div>
        </div>
    </div>
@endsection

