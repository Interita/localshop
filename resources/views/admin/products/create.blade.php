@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            @lang('base.product')
        </h1>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">

            <div class="box-body">
                <div class="row">
                    {!! Form::open(['route' => 'products.store', 'files'=>true]) !!}

                        @include('admin.products.fields')

                        <!-- Submit Field -->
                        <div class="form-group col-sm-12">
                            {!! Form::submit(Lang::get('base.save'), ['class' => 'btn btn-primary']) !!}
                            <a href="{!! route('products.index') !!}" class="btn btn-default">@lang('base.cancel')</a>
                        </div>

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
