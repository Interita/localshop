<!-- Measures Field -->
<div class="form-group col-sm-2">

    <a class="btn btn-primary buttons-group" href="{{ route('products.index') }}">Visi Produktai</a>
</div>

<!-- Categories Field -->
<div class="form-group col-sm-2">
    {!! Form::label('category_id', 'Kategorijos:') !!}
    {{ Form::select(
        'category_id',
        $categories,
        null,
        ['class'=>'form-control', 'placeholder' => '', 'onchange' => 'location = "products?category="+this.value', 'placeholder'=>'--- Pasirinkite ---']) 
    }}
</div> 

<!-- Measures Field -->
<div class="form-group col-sm-2">
    {!! Form::label('measure_id', 'Išmatavimai:') !!}
    {{ Form::select(
        'measure_id',
        $measures,
        null,
        ['class'=>'form-control', 'placeholder' => '', 'onchange' => 'location = "products?measure="+this.value', 'placeholder'=>'--- Pasirinkite ---']) 
    }}
</div>  

<!-- Filtruoti Field -->
<div class="form-group col-sm-2">
    <a class="btn btn-primary buttons-group" href="#">Filtruoti</a>
</div>



