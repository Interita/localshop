<!-- Categories Field -->
<div class="form-group col-sm-12">
    {!! Form::label('categories', 'Kategorijos:') !!}
    @foreach($categories as $category)
        <div class="category">
            {{ $category['title'] }} {{ Form::checkbox('categories[]', $category['id'], $category['selected'] ) }}
        </div>
    @endforeach
    
</div>