@if(count($categories))
	<ul>
	    @foreach($categories as $category)
	        <li class="category">
	        	@php 
	        		$checkbox_cat_id = 'category'.$category->id
	        	@endphp
	        	{{ Form::checkbox('categories[]', $category->id, null, ['id' => $checkbox_cat_id] ) }}
	        	{!! Form::label($checkbox_cat_id, $category->title) !!}
	        	
	        </li>
	        @include('admin.products.categories_and_childs',['categories' => $category->childs])
	    @endforeach
	</ul>
@endif
