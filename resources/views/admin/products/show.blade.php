@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            @lang('base.product'): {{ $product->title }}
        </h1>
    </section>
    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    @include('admin.products.show_fields')

                    <div class="clearfix"></div>
                    
                    <a href="{!! route('products.index') !!}" class="btn btn-default">@lang('base.back')</a>
                </div>
            </div>
        </div>
    </div>
@endsection
