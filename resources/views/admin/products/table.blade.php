<table class="table table-responsive" id="products-table">
    <thead>
        <tr>
            <th>Pavadinimas</th>
            <th>Kategorijos</th>
            <th>Sku</th>
            <th>Barkodas</th>
            <th>Pirkimo Kaina</th>
            <th>Kaina</th>
            <th>Kaina Pvm</th>
            <th>Pvm Grupė</th>
            <th>Kiekis</th>
            <th>Matas</th>
            <th>Paveikslėlis</th>
            <th colspan="3"></th>
        </tr>
    </thead>
    <tbody>
    @foreach($products as $product)
        <tr>
            <td>{!! $product->title !!}</td>
            <td>
                @foreach($product->categories as $category)
                    <a href="{{route('categories.index')}}/{{$category->id}}">
                        <div class="categories-inline">{{ $category->title }}</div>
                    </a>
                @endforeach
            </td>
            <td>{!! $product->sku !!}</td>
            <td>{!! $product->barcode !!}</td>
            <td>{{ $product->price_purchase }}</td>
            <td>{!! number_format($product->price, 2) !!}</td>
            <td>{{ $product->price_pvm }}</td>
            <td>{{ $product->pvm_group['title'] }}</td>
            <td>{!! $product->count !!}</td>
            <td>{{ $product->measure['title'] }}</td>
            <td>
                @if( !empty($product->image) )
                    <img class="small-image" src="{{ asset('storage') }}/{{ $product->image }}">
                @endif
            </td>
            <td>
                {!! Form::open(['route' => ['products.destroy', $product->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('products.show', [$product->id]) !!}" class='btn btn-default'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('products.edit', [$product->id]) !!}" class='btn btn-default'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>