@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            @lang('base.product'): {{ $product->title }}
        </h1>
   </section>
   <div class="content">
       @include('flash::message')
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($product, ['route' => ['products.update', $product->id], 'method' => 'patch', 'files'=>true]) !!}

                        @include('admin.products.fields')

                        <!-- Submit Field -->
                        <div class="form-group col-sm-12">
                            {!! Form::submit(Lang::get('base.update'), ['class' => 'btn btn-primary']) !!}
                            <a href="{!! route('products.index') !!}" class="btn btn-default">@lang('base.cancel')</a>
                        </div>                        

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection