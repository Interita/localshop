@extends('layouts.app')


@section('scripts')
    <script>
        $( function() {

            //-------- DATATABLE ------------
            $('#data_table').DataTable( {
                "order": [[ 0, "desc" ]], //Prod ID
                "iDisplayLength": 50,
                "language": {
                    "lengthMenu": "Rodyti _MENU_ per puslapį",
                    "search": "Ieškoti prekių:",
                }
            } );
            //------------------------------


            $( "#datepicker1" ).datepicker();
            $( "#datepicker1" ).datepicker(
                "option", 
                "dateFormat", 
                "yy-mm-dd",
            );
            $( "#datepicker1" ).datepicker('setDate', $("#datepicker1").attr('date') );


            $( "#datepicker2" ).datepicker();
            $( "#datepicker2" ).datepicker(
                "option", 
                "dateFormat", 
                "yy-mm-dd"
            ); 
            $( "#datepicker2" ).datepicker('setDate', $("#datepicker2").attr('date') );           
        });         
    </script>
@endsection


@section('content')
    <section class="content-header">
        <h1 class="pull-left">
            @if($type_id === 1) 
                @lang('layouts/menu.report_purchase') 
            @endif

            @if($type_id === 2) 
                @lang('layouts/menu.report_sale') 
            @endif            
        </h1>

    </section>
    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-body">


                <!-- DATOS FILTRAS -->
                <form action="" method="GET" role="form">
                    <div class="date-pickers">
                        <span>Nuo: <input type="text" id="datepicker1" name="nuo" value="" date="{{ $date_from }}"></span>
                        <span>Iki: <input type="text" id="datepicker2" name="iki" value="" date="{{ $date_to }}"></span>

                        <span><button class="btn btn-default">Filtruoti</button></span>
                    </div>               
                </form>


                <table id="data_table" class="table table-hover">
                    <thead>
                        <tr>
                            <th>@lang('base.product_id')</th>
                            <th>@lang('base.product')</th>
                            <th>@lang('base.product_code')</th>
                            <th>@lang('base.count')</th>
                            <th>@lang('base.price_pvm')</th>
                            <th>@lang('base.total')</th>
                            <th>@lang('base.type')</th>
                            <th>@lang('base.user')</th>
                      
                        </tr>
                    </thead>

                    <tbody>
                        @foreach($report_products as $product)
                            <tr>
                                <td>{{ $product['product_id'] }}</td>
                                <td @if($product['deleted']) class="deleted-product" @endif>
                                    {{ $product['title'] }}
                                </td>
                                <td @if($product['deleted']) class="deleted-product" @endif>
                                    {{ $product['sku'] }}
                                </td>
                                <td>{{ $product['count'] }}</td>
                                <td>{{ $product['price'] }}</td>
                                <td>{{ $product['total'] }}</td>
                                <td>{{ $product['type'] }}</td>
                                <td>{{ $product['user'] }}</td>
                               
                            </tr>
                        @endforeach
                    </tbody>

                    <tfoot>
                        <tr>
                            <td colspan="3"><b>VISO</b></td>
                            <td colspan="2"><b>{{ $total_count }}</b></td>
                            <td colspan="3"><b>{{ $total_price }}</b></td>
                        </tr>
                    </tfoot>                    
                </table>

            </div>
        </div>
    </div>
@endsection

