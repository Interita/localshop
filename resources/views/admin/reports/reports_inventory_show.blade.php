@extends('layouts.app')

@section('scripts')
    <script>
        $( function() {

            //-------- DATATABLE ------------
            $('#data_table').DataTable( {
                "order": [[ 0, "asc" ]], //Prod
                "iDisplayLength": 50,
                "language": {
                    "lengthMenu": "Rodyti _MENU_ per puslapį",
                    "search": "Ieškoti prekių:",
                }
            } );
            //------------------------------           
        });         
    </script>
@endsection


@section('content')
    <section class="content-header">
        <h1>
            Ataskaitos: Inventorizacija <b>{{ $inventory_activity->title }}</b>
        </h1>
    </section>
    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-left: 20px; padding-bottom: 50px">

					<!-- Id Field -->
					<div class="form-group">
					    {!! Form::label('title', 'Pavadinimas:') !!}
					    <p>{!! $inventory_activity->title !!}</p>
					</div>

					<!-- User Field -->
					<div class="form-group">
					    {!! Form::label('user_id', 'Vartotojas:') !!}
					    <p>{!! $inventory_activity->user->name !!}</p>
					</div>						

					<!-- Date Field -->
					<div class="form-group">
					    {!! Form::label('created_at', 'Data:') !!}
					    <p>{!! $inventory_activity->created_at !!}</p>
					</div>					

                    <a href="{!! route('reports.inventory') !!}" class="btn btn-default">Atgal</a>
                </div>



             
                    <table id="data_table" class="table table-hover">
                        <thead>
                            <tr>
                                <th>Produktas</th>
                                <th>Kiekis</th>
                                <th>Kiekis Realus</th>
                                <th>Trūkumas</th>
                                <th>Kaina</th>
                                <th>Trūkstama Suma</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($inventory_activity->items as $item)
                                <tr>
                                    <td>{{ $item->product->title }}</td>
                                    <td>{{ $item->count }}</td>
                                    <td>{{ $item->count_real }}</td>
                                    <td @if($item->shortage != 0) class="shortage" @endif>{{ $item->shortage }}</td>
                                    <td>{{ $item->price }}</td>
									<td>{{ $item->shortage_total }}</td>
                                    
                                </tr>
                            @endforeach
                        </tbody>
	                    <tfoot>
	                        <tr>
	                            <td colspan="3"><b>VISO</b></td>
	                            <td colspan="2"><b>{{ $shortage_total_count }}</b></td>
	                            <td colspan="3"><b>{{ $shortage_total_price }}</b></td>
	                        </tr>
	                    </tfoot>                         
                    </table>
            


            </div>
        </div>
    </div>
@endsection
