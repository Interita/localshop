@if(count($categories))

	@php $prefix .= ' - ' @endphp 
	

    @foreach($categories as $category)

		@if($category->parent_id === 0)
			@php $prefix = '' @endphp 
		@endif

		@if($category->id === $parent_id)
			@php $selected = 'selected' @endphp 
		@else
			@php $selected = '' @endphp 
		@endif		

        <option {{ $selected }} value="{{ $category->id }}">
        	{{ $prefix }}{{ $category->title }}
		</option>     
		@include('admin.categories.categories_and_childs_select', ['categories' => $category->childs])    
    @endforeach
	
@endif