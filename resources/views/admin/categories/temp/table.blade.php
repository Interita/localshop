<table class="table table-responsive" id="categories-table">
    <thead>
        <tr>
            <th>ID</th>
            <th>Title</th>
            <th>Parent Id</th>
            <th>Image</th>
            <th>Ordering</th>
            <th colspan="3">Action</th>
        </tr>
    </thead>
    <tbody>

        @include('categories.categories_and_childs_table', ['categories' => $categories])

    @foreach($categories as $category)

        <tr>
            <td>{!! $category->id !!}</td>
            <td>{!! $category->title !!}</td>
            <td>{!! $category->parent_id !!}</td>
            <td>
                @if( !empty($category->image) )
                    <img class="small-image" src="{{ Storage::url($category->image) }}">
                @endif
            </td>
            <td>{!! $category->ordering !!}</td>
            <td>
                {!! Form::open(['route' => ['categories.destroy', $category->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('categories.show', [$category->id]) !!}" class='btn btn-default'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('categories.edit', [$category->id]) !!}" class='btn btn-default'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>


@include('categories.categories_and_childs', ['categories' => $categories])