<!-- Title Field -->
<div class="form-group col-sm-6">
    {!! Form::label('title', Lang::get('base.title').':') !!}
    {!! Form::text('title', null, ['class' => 'form-control']) !!}
</div>


<!-- Categories parent Field -->
<div class="form-group col-sm-6">
    {!! Form::label('category_id', Lang::get('base.parent_category').':') !!}

    <select class="form-control" name="parent_id">
        <option value="0"></option>         
        @include('admin.categories.categories_and_childs_select', ['parent_id' => $parent_id, 'categories' => $categories, 'prefix' => ''])  
    </select>
</div>



<!-- Image Field -->
<div class="form-group col-sm-6">
    {!! Form::label('image', Lang::get('base.image').':') !!}
    @if( !empty($category->image) )
        <div class="category-image">
            <img class="small-image" src="{{ asset('storage') }}/{{ $category->image }}">
        </div>
    @endif
    {!! Form::file('image', null, ['class' => 'form-control']) !!}
</div>


<!-- Ordering Field -->
<div class="form-group col-sm-6">
    {!! Form::label('ordering', Lang::get('base.ordering').':') !!}
    {!! Form::text('ordering', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit(Lang::get('base.save'), ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('categories.index') !!}" class="btn btn-default">@lang('base.cancel')</a>
</div>
