<!-- Supplier Document ID Field -->
<div class="col-sm-1">						
	<div class="supplier-document-status @if($document_processing)finished @endif"></div>
</div>	

<!-- Suppliers Groups Field -->
<div class="col-sm-4">
	<div class="form-group">
		@php 
			$supplier_id = isset($supplier_document->supplier_id) ? $supplier_document->supplier_id : null;
		@endphp

    	{!! Form::label('supplier_id', Lang::get('base.supplier').':') !!}
    	{{ Form::select('supplier_id', $suppliers, $supplier_id, ['class'=>'form-control', 'disabled'=>$document_processing]) }}
    </div>
</div>

<!-- Document Nr Field -->
<div class="col-sm-4">
	<div class="form-group">
		@php 
			$document_nr = isset($supplier_document->nr) ? $supplier_document->nr : null 
		@endphp

	    {!! Form::label('document_nr', Lang::get('base.document_nr').':') !!} 
	    {!! Form::text('document_nr', $document_nr, ['class' => 'form-control', 'required'=>true, 'disabled'=>$document_processing]) !!}
	    
	</div>
</div>	


<div class="col-sm-3">
	<div class="buttons-group pull-left">
	    {!! Form::button('Vykdyti', ['class' => 'btn btn-success', 'id' => 'process_supplier', 'disabled' => $document_processing]) !!}
	</div>								
	<div class="buttons-group pull-right">
	    
	    {!! Form::button('Užbaigti', ['class' => 'btn btn-danger', 'id' => 'finish_process_supplier', 'disabled' => !$document_processing]) !!}
	</div>								
</div>