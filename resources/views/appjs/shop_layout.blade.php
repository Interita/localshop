<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Point Of Sale</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    
    @if (Auth::check())
        <meta name="api-token" content="{{ Auth::user()->api_token }}">
    @endif

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">


    <!-- custom styles -->
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <link rel="stylesheet" href="{{ asset('css/frontend.css') }}">

</head>

<body>

    @yield('content')


    @yield('top_scripts')
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    @yield('bottom_scripts')
    
</body>
</html>