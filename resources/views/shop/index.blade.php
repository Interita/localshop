@extends('layouts.shop')

@section('content')

<div id="app" v-cloak class="content">
	<input id="storage-url" type="hidden" value="{{ url('/')  }}{{ Storage::url('') }}">
	<input id="logged-user-id" type="hidden" value="{{ Auth::id() }}">
	<input id="pvm-rate" type="hidden" value="{{ env('PVM') }}">

	<div class="front-left-side">

		<div class="container-fluid">
			<div class="row">
				<div class="message" v-html="message"></div>
				<products-list v-bind:products_list="products_list"></products-list>
			</div>
		</div>

		<div class="calculator-block">
			<div>
				<button class="reload-screen" v-on:click="reload_screen()">@lang('shop.cancel')</button>
				<div class="calculator-number pull-right"><% calculator_number %></div>
			</div>

			<div class="calculator">
				<div class="calculator-header">
					
					<div class="calculator-left">
						<div class="customer">
							<div class="circle pull-left">
								<i class="fa fa-user"></i>
							</div>
							<div class="customer-text pull-left">
								@lang('shop.customer')
							</div>
						</div>
					</div>
					<div class="calculator-right">
						<div class="discount-card">
							<div class="discount-card-left">
								<div class="text">@lang('shop.card_accepted')</div>
							</div>
							<div class="discount-card-right">
								<div class="color"></div>
							</div>
						</div>
					</div>	
				</div>

				<div class="clearfix"></div>

				<div class="calculator-left">

					<div class="payment">
						<a class="" data-toggle="modal" href='#payment-modal-id'>
							<i class="fa fa-chevron-circle-right"></i>
							<div class="text">@lang('shop.pay')</div>
						</a>
					</div>				
				</div>
				<div class="calculator-right">
						<div class="buttons">
						<div class="button btn" v-on:click="add_calculator_number(1)">1</div>
						<div class="button btn" v-on:click="add_calculator_number(2)">2</div>
						<div class="button btn" v-on:click="add_calculator_number(3)">3</div>
						<!--<div class="button btn" :class="quantity_class" v-on:click="click_quantity()">-->
						<div class="button btn" v-on:click="add_discount_percentage_num(5)">-5%</div>
						<div class="button btn" v-on:click="add_calculator_number(4)">4</div>
						<div class="button btn" v-on:click="add_calculator_number(5)">5</div>
						<div class="button btn" v-on:click="add_calculator_number(6)">6</div>
						<div class="button btn"  v-on:click="add_discount_percentage_num(10)">
							<!--<span class="empty"></span>-->
							-10%
						</div>
							
						
						<div class="button btn" v-on:click="add_calculator_number(7)">7</div>
						<div class="button btn" v-on:click="add_calculator_number(8)">8</div>
						<div class="button btn" v-on:click="add_calculator_number(9)">9</div>
						<div class="button btn" v-on:click="add_discount_percentage()">disc</div>
						<div class="button btn" v-on:click="delete_calculator_number()">C</div>
						<div class="button btn" v-on:click="add_calculator_number(0)">0</div>
						<div class="button btn" v-on:click="add_calculator_number('.')">.</div>
						<div class="button btn" v-on:click="add_discount_percentage()">-%</div>
					</div>					
				</div>			
			</div>
		</div>
	</div>

	<div class="front-right-side">
		<div class="container-fluid">

			<!-- BREADCRUMB -->
			<div class="row">
				<div class="right-side-top">
					<ul class="breadcrumb">
						<li>
							<span class="fa fa-home" aria-hidden="true" v-on:click="go_home()"></span>
						</li>
						<li v-for="item in breadcrumb" v-on:click="category_click(item.id)">
							<% item.title %>
						</li>
					</ul>

					<!-- SEARCH -->
					<div class="search-block">
						<input class="search-products" type="text" value="" placeholder="@lang('shop.search_products')" v-model="search">
						<button class="btn btn-default" v-on:click="clear_search()">@lang('shop.search_clean')</button>
					</div>
					
				</div>
			</div>

			<!-- CATEGORIES -->
			<div class="row">
				<div class="col-sm-4 col-md-4 col-lg-3" v-for="category in categories">
					<category-block 
						v-bind:category="category" 
						v-bind:category_click="category_click"
						v-bind:storage_url="storage_url"
					></category-block>
				</div>
			</div>

			<div class="row">
				<div class="title">
					<h2>@lang('shop.products')</h2>
				</div>
			</div>			

			<!-- PRODUCTS -->
			<div class="row" >
				<div class="col-sm-4 col-md-3 col-lg-2" v-for="product in filtered_products.slice(0, 100)">
					<product-block 
						v-bind:product="product" 
						v-bind:product_click="product_click"
						v-bind:storage_url="storage_url"
						v-on:test_func="test_func"
					></product-block>
				</div>
			</div>


		</div>
	</div>

	<!-- ----------------- MODAL -------------------- -->
	{{-- @include('shop.payment_old') --}}
	@include('shop.payment')


</div>	


@endsection