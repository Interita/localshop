
<div class="modal fade" id="payment-modal-id">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">

			<div class="modal-header">
				<div class="row">
					<div class="col-sm-4">
						<button type="button" data-dismiss="modal" aria-hidden="true">@lang('shop.back')</button>
					</div>	
					<div class="col-sm-8">
						<h2 class="modal-title">@lang('shop.payment')</h2>
					</div>
				</div>			
			</div>

			<div class="modal-body">


				<div class="row">
					<div class="payment-left col-sm-4">
						<div>
							<button class="btn btn-default btn-payment" v-on:click="disable_card">@lang('shop.cash')</button>
						</div>
						<div>
							<button class="btn btn-default btn-payment" v-on:click="enable_card">@lang('shop.card')</button>
						</div>
						<div>
							<button disabled class="btn btn-default btn-payment">@lang('shop.gift_coupon')</button>
						</div>
						<div>
							<button disabled class="btn btn-default btn-payment">@lang('shop.credit')</button>
						</div>
						<div>
							<button disabled class="btn btn-default btn-payment">@lang('shop.customer')</button>
						</div>
						<div>
							<button disabled class="btn btn-default btn-payment">@lang('shop.receipt')</button>
						</div>
						<div>
							<button disabled class="btn btn-default btn-payment">@lang('shop.receipt_by_mail')</button>
						</div>
					</div>
					<div class="payment-right col-sm-8">
						<div class="price-total">
							@lang('shop.total_price'): <span class="pull-right"><% price_total %></span>
						</div>
						<div class="price-change">
							@lang('shop.change'): <span class="pull-right"><% money_change_string %></span>
						</div>

						

						<div class="payment-row row">
							<div class="col-sm-6">

								<div class="calculator-modal">
									
									<div v-if="card_enable">
										<input disabled type="text" class="form-control" id="payment-card" v-bind:value="money_card">
									</div>
									<div v-else>
										<input disabled type="text" class="form-control" id="payment-cash" v-model="money_string">
									</div>

									<div v-if="card_enable"></div>
									<div v-else>										
										<div class="buttons">
											<div class="button btn btn-default" v-on:click="enter_money2('1')">1</div>
											<div class="button btn btn-default" v-on:click="enter_money2('2')">2</div>
											<div class="button btn btn-default" v-on:click="enter_money2('3')">3</div>
											<div class="button btn btn-default" v-on:click="enter_money2('4')">4</div>
											<div class="button btn btn-default" v-on:click="enter_money2('5')">5</div>
											<div class="button btn btn-default" v-on:click="enter_money2('6')">6</div>
											<div class="button btn btn-default" v-on:click="enter_money2('7')">7</div>
											<div class="button btn btn-default" v-on:click="enter_money2('8')">8</div>
											<div class="button btn btn-default" v-on:click="enter_money2('9')">9</div>
											<div class="button btn btn-default" v-on:click="delete_money2()">C</div>
											<div class="button btn btn-default" v-on:click="enter_money2('0')">0</div>
											<div class="button btn btn-default" v-on:click="enter_money2('.')">,</div>
										</div>
									</div>
								</div>

							</div>
							<div class="col-sm-6">
								<div v-if="show_pay_button3">
									<div v-if="card_enable">
										<button class="btn btn-success btn-pay" v-on:click="save_order_card()">@lang('shop.pay')</button>
									</div>
									<div v-else>
										<button class="btn btn-success btn-pay" v-on:click="save_order2()">@lang('shop.pay')</button>
									</div>
								</div>
								<div v-else>
									<button class="btn btn-success btn-pay" disabled>@lang('shop.pay')</button>
								</div>								
							</div>	

						</div>


					</div>
				</div>
				

			</div>
		</div>
	</div>
</div>