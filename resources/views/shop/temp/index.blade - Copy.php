@extends('layouts.shop')

@section('content')


<div id="app" v-cloak class="content">
	<input id="storage-url" type="hidden" value="{{ url('/')  }}{{ Storage::url('') }}">
	<input id="logged-user-id" type="hidden" value="{{ Auth::id() }}">
	<input id="pvm-rate" type="hidden" value="{{ env('PVM') }}">

	<div class="front-left-side">
		<div class="container-fluid">

			<div class="row">
				<div class="message" v-html="message"></div>

				
				<products-list v-bind:products_list="products_list"></products-list>

			</div>
		</div>


		<div class="calculator-block">

			<div class="price-total">
				Viso: <% price_total %>
				<button class="reload-screen" v-on:click="reload_screen()">Atnaujinti</button>
			</div>

			<div class="calculator">
				<div class="calculator-left">
					<div class="customer">
						<i class="fa fa-user"></i>Customer
					</div>
					<div class="payment">
						<a class="btn btn-primary" data-toggle="modal" href='#payment-modal-id'>Payment</a>
					</div>				
				</div>
				<div class="calculator-right">
						<div class="buttons">
						<div class="button btn btn-default" v-on:click="product_add_qty(1)">1</div>
						<div class="button btn btn-default" v-on:click="product_add_qty(2)">2</div>
						<div class="button btn btn-default" v-on:click="product_add_qty(3)">3</div>
						<div class="button btn btn-default" :class="quantity_class" v-on:click="click_quantity()">Qty</div>
						<div class="button btn btn-default" v-on:click="product_add_qty(4)">4</div>
						<div class="button btn btn-default" v-on:click="product_add_qty(5)">5</div>
						<div class="button btn btn-default" v-on:click="product_add_qty(6)">6</div>
						<div class="button btn btn-default">Disc</div>
						<div class="button btn btn-default" v-on:click="product_add_qty(7)">7</div>
						<div class="button btn btn-default" v-on:click="product_add_qty(8)">8</div>
						<div class="button btn btn-default" v-on:click="product_add_qty(9)">9</div>
						<div class="button btn btn-default">Price</div>
						<div class="button btn btn-default">+/-</div>
						<div class="button btn btn-default" v-on:click="product_add_qty(0)">0</div>
						<div class="button btn btn-default">.</div>
						<div class="button btn btn-default">X</div>
					</div>					
				</div>			
			</div>
		</div>
	</div>

	<div class="front-right-side">
		<div class="container-fluid">

			<!-- BREADCRUMB -->
			<div class="row">
				<div class="right-side-top">
					<ul class="breadcrumb">
						<li><span class="icon glyphicon glyphicon-home" aria-hidden="true" v-on:click="go_home()"></span></li>
						<li v-for="item in breadcrumb" v-on:click="category_click(item.id)">
							<% item.title %>
							
						</li>
					</ul>

					<!-- SEARCH -->
					<div class="search-block">
						<input class="search-products" type="text" value="" placeholder="ieškoti produktų" v-model="search">
						<button class="btn btn-default" v-on:click="clear_search()">Išvalyti</button>
					</div>
					
				</div>
			</div>

			<!-- CATEGORIES -->
			<div class="row">
				<div class="col-sm-6 col-md-4 col-lg-3" v-for="category in categories">
					<category-block 
						v-bind:category="category" 
						v-bind:category_click="category_click"
						v-bind:storage_url="storage_url"
					></category-block>
				</div>
			</div>

			<div class="row">
				<div class="title"><h2>Produktai</h2></div>
			</div>			

			<!-- PRODUCTS -->
			<div class="row" >
				<div class="col-sm-4 col-md-3 col-lg-2" v-for="product in filtered_products.slice(0, 6)">
					<product-block 
						v-bind:product="product" 
						v-bind:product_click="product_click"
						v-bind:storage_url="storage_url"
						v-on:test_func="test_func"
					></product-block>
				</div>
			</div>


		</div>
	</div>


	<!-- ----------------- MODAL -------------------- -->

	<div class="modal fade" id="payment-modal-id">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<div class="message" v-html="message"></div>
				</div>
				<div class="modal-body">


					<div v-if="products_list.length == 0">
						<div class="price-change"><b>Graža:</b> <% money_change2 %></div>
					</div>

					<div class="row" v-else>

						<div class="col-sm-6">
							<div class="price-total"><b>Mokėti:</b> <% price_total %></div>
						</div>

						<div class="col-sm-6">
							<div class="price-change"><b>Graža:</b> <% money_change %></div>
						</div>


						<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
						    <div class="form-group">
							    <label for="usr">Gryni:</label>
							    <input disabled type="text" class="form-control" id="payment-cash" v-model="money_cash">
						    </div>						
						</div>
						<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
						    <div class="form-group">
							    <label for="usr">Kortelė:</label>
							    <input type="checkbox" name="card-enable" value="1" v-on:click="enable_card">Enable Card<br>
							    <input disabled type="text" class="form-control" id="payment-card" v-model="money_card">
						    </div>							
						</div>

						<div class="col-xs-12">
							<div class="calculator-modal">

									<div class="buttons">
										<div class="button btn btn-default" v-on:click="enter_money(1)">1</div>
										<div class="button btn btn-default" v-on:click="enter_money(2)">2</div>
										<div class="button btn btn-default" v-on:click="enter_money(3)">3</div>
										<div class="button btn btn-default" v-on:click="enter_money(4)">4</div>
										<div class="button btn btn-default" v-on:click="enter_money(5)">5</div>
										<div class="button btn btn-default" v-on:click="enter_money(6)">6</div>
										<div class="button btn btn-default" v-on:click="enter_money(7)">7</div>
										<div class="button btn btn-default" v-on:click="enter_money(8)">8</div>
										<div class="button btn btn-default" v-on:click="enter_money(9)">9</div>
										<div class="button btn btn-default" v-on:click="enter_money(0)">0</div>
										<div class="button btn btn-default" v-on:click="delete_money()">Del</div>
										<div class="button btn btn-default" v-on:click="enter_money(100)">00</div>
									</div>
								</div>

						</div>

						<div class="col-xs-12">
							<div v-if="show_pay_button">
								<button class="btn btn-success btn-pay" v-on:click="save_order()">PAY</button>
							</div>
						</div>
					</div>



					
				</div>
				<div class="modal-footer">

				</div>
			</div>
		</div>
	</div>

</div>	





@endsection