@extends('layouts.point_of_sale_app')

@section('content')

<div id="point-of-sale" v-cloak class="content">
	<input id="storage-url" type="hidden" value="{{ url('/')  }}{{ Storage::url('') }}">
	<input id="logged-user-id" type="hidden" value="{{ Auth::id() }}">
	<input id="pvm-rate" type="hidden" value="{{ env('PVM') }}">

	<div class="front-left-side">

		<div class="container-fluid products-block">
			<div class="row">
				<div class="message" v-html="message_after_order"></div>
				<cart 
					v-bind:products="$store.getters['cart/products']"
					v-on:on_add_product="on_add_product"
					v-on:on_remove_product="on_remove_product"
					v-on:on_cart_item_click="on_cart_item_click"
				></cart>
			</div>
		</div>

		<calculator 
			v-on:on_reload_screen="on_reload_screen"
			v-on:add_discount_percentage="on_product_discount_percentage"
			v-on:on_open_checkout="on_open_checkout"
		></calculator>	

		<checkout
			v-bind:total_products_price="total_products_price"
			v-bind:order_processing="order_processing"
			v-on:on_save_order="on_save_order"
		></checkout>	

	</div>

	<div class="front-right-side">
		<div class="container-fluid">

			<!-- BREADCRUMB -->
			<div class="row breadcrumb-row">
				<div class="right-side-top">
					<breadcrumb 
						v-bind:breadcrumb="$store.getters['breadcrumb/breadcrumb']" 
						v-on:on_breadcrumb_click="on_breadcrumb_click"
						v-on:on_click_go_home="on_click_go_home"
					></breadcrumb>

					<!-- SEARCH -->
					<div class="search-block">
						<input class="search-products" type="text" value="" placeholder="@lang('shop.search_products')" v-model="search">
						<button class="btn btn-default" v-on:click="clear_search()">@lang('shop.search_clean')</button>
					</div>
				</div>
			</div>

			<!-- CATEGORIES -->
			<div class="row categories-row">
				<categories 
					v-bind:categories="$store.getters['categoriesList/categories']" 
					v-on:on_category_click="on_category_click"
				></categories>
			</div>

			<div class="row">
				<div class="title">
					<h2>@lang('shop.products')</h2>
				</div>
			</div>			

			<!-- PRODUCTS -->
			<div class="row products-row" >
				<products 
					v-bind:products="$store.getters['productsList/products']" 
					v-on:on_product_click="on_product_click"
				></products>
			</div>


		</div>
	</div>


</div>	


@endsection