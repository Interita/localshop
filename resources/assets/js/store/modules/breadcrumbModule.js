const breadcrumbModule = {

	namespaced: true,

	state: {
		parent: 0, //category_id
		breadcrumb: [],
	},

	getters: {
		breadcrumb(state){
			return state.breadcrumb;
		}
	},

	mutations: {
		fetch_breadcrumb(state, parent){
			state.parent = parent;
            axios.get('get_breadcrumb/' + parent).then((res) => {
                state.breadcrumb = res.data;
                state.breadcrumb.reverse();
            });                
		}
	},

	actions: {
		fetch_breadcrumb(context, parent=0){
			context.commit('fetch_breadcrumb', parent)
		}
	}
}


export default breadcrumbModule