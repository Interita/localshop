
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

//import VueNumeric from 'vue-numeric'

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

 //delimiters: ['<%','%>'],

Vue.component('example-component', require('./components/ExampleComponent.vue'));

Vue.component('product-block', require('./components/ProductBlockComponent.vue'));
Vue.component('category-block', require('./components/CategoryBlockComponent.vue'));
Vue.component('products-list', require('./components/ProductsListComponent.vue'));

//Vue.component('vue-numeric', VueNumeric);


const app = new Vue({
    el: '#app',
    delimiters: ['<%','%>'],
    mounted() {
        //console.log('App started')
        this.get_categories();
        this.get_products();
        this.get_cart_products();
    },    
    data: {
            user_id: document.getElementById('logged-user-id').value,
            pvm_rate: document.getElementById('pvm-rate').value,
            storage_url: document.getElementById('storage-url').value,
            categories: [],
            products: [],
            //filtered_products: [],
            products_list: [],
            breadcrumb: [],
            message: '',

            quantity_selected: false,
            quantity_class: '',

            cents_cash: 0,
            card_enable: false,
            money_change2: 0.00,
            money_string: '0',

            //pay_button_visible: false,

            search: '', 
            calculator_number: '',
    },

    computed: {

        price_total(){
            var price_total = 0

            this.products_list.forEach(function(product){
                var price = app.product_price(product);
                price_total += product.count * price;
                //price_total += product.count * product.price;
            }); 

            return price_total.toFixed(2);
        },
        money_cash(){
            return this.cents_cash / 100;
        },
        money_card(){

            if(this.card_enable){
                var card_money = this.price_total - this.money_cash;

                if(card_money > 0){
                    card_money = card_money.toFixed(2);
                    card_money = parseFloat(card_money);
                    return card_money;
                    
                    //return card_money.toFixed(2);
                }
            }

            return 0;
        },        
        money_change(){
            var money = this.money_cash + this.money_card;
            var money_change = money - this.price_total

            if(money_change > 0)
                return money_change.toFixed(2);

            return 0;           
        },
        money_change_string(){
            var money = parseFloat(this.money_string);
            var money_change = money - this.price_total;
            //console.log(this.money_string);
            //console.log(money);

            if(money_change > 0){
                return money_change.toFixed(2);
                //return money_change;
            }

            return 0;           
        },        
        show_pay_button(){
            var money = this.money_cash + this.money_card;
            var balance = money - this.price_total;

            //console.log(money);
            //console.log(balance);

            if(balance >= 0)
                return true;

            return false;
        },
        /*
        show_pay_button2(){
            var money = parseFloat(this.money_string);
            var balance = money - this.price_total;

            if(balance >= 0 && this.price_total > 0)
                return true;

            return false;
        },   
        */
        show_pay_button3(){
            var money = parseFloat(this.money_string);
            var balance_cash = money - this.price_total;
            var balance_card = this.money_card - this.price_total;

            if(balance_cash >= 0 && this.price_total > 0)
                return true;

            if(balance_card >= 0 && this.price_total > 0)
                return true;            

            return false;
        },              
        filtered_products(){
            return this.products
                .filter((product) => {

                    return  product.title.toUpperCase().match(this.search.toUpperCase()) 
                            || product.sku.match(this.search)
                            || (product.barcode != null && product.barcode.match(this.search) )

                });
        },
    },

    watch: {
        
        products_list: function(){
            
            //console.log('products_list');

            this.products_list.forEach(function(product){
                product.price = parseFloat(product.price);
                product.price = product.price.toFixed(2);

                product.total = parseFloat(product.total);
                product.total = product.total.toFixed(2);
            });
        }
        

        /*
        products_list: {
            handler: function(){
                console.log('change');

                
            },
            deep: true
        }
        */
    },

    methods: {

        test_func(){
            console.log('test emit');
        },
        test_btn_func(){
            console.log('test_btn_func');
        },   

        get_logged_user_id(){
            axios.get('get_user_id').then((res) => {
                var user_id = res.data;
                console.log(user_id);
            });             
        },

        redirect_if_not_logged(){
            axios.get('get_user_id').then((res) => {
                var user_id = res.data;
                if(user_id === 0)
                    window.location.href = "login";
            });             
        },        

        product_price(product){
            var price = product.price;

            if(product.price_discounted)
                price = product.price_discounted;

            if(product.discount_percentage){
                var discount = ( parseFloat(product.price) * parseFloat(product.discount_percentage) ) / 100;
                var new_price = product.price - discount;
                new_price = new_price.toFixed(2);
                price = new_price;                    
            }

            return price;
        },        

        reload_screen(){
            this.redirect_if_not_logged();

            this.empty_cart();
            this.clear_search();
            this.get_categories();
            this.get_products();
            this.get_cart_products();    
            this.products_list = [];
            this.clear_operation();
            this.calculator_number = "";
            this.money_string = '0';
            this.go_home();
        },

        clear_search(){
            this.redirect_if_not_logged();
            
            this.search = '';
        },

        enable_card(){
            //if(this.card_enable)
            //    this.card_enable = false;
            //else
                this.card_enable = true;
        },

        disable_card(){
            //if(this.card_enable)
            //    this.card_enable = false;
            //else
                this.card_enable = false;
        },        

        enter_money(num){
            if(this.cents_cash == 0)
                this.cents_cash = num;
            else{
                if(num == 100)
                    this.cents_cash = this.cents_cash * num;
                else
                    this.cents_cash = (this.cents_cash * 10) + num;
            }
        },

        
        enter_money2(num){

            var dot = '.';
            var exists_dot = this.money_string.includes(dot);
            var after_dot = this.money_string.split(".")[1];

            if(after_dot === undefined || after_dot.length < 2){ //2sk po kablelio

                if(num === dot && exists_dot === true){
                    //antras kablelis

                }
                else{
                    //console.log(this.money_string);
                    if(this.money_string === '0' && num != dot)
                        this.money_string = num;
                    else{
                        this.money_string += num;
                    }                
                }                
            }
        }, 

        delete_money2(){
            this.money_string = '0'; 
        },              

        delete_money(){
            this.cents_cash = 0; 
        },

        click_quantity(){
            //alert();
            if( this.quantity_selected ){
                this.quantity_class = '';
                this.quantity_selected = false;
            }
            else{
                this.quantity_class = 'selected';
                this.quantity_selected = true;
            }
        }, 

        product_add_qty(qty){
            
            if(this.quantity_selected){

                //var product;

                this.products_list.forEach(function(product){
                    if(product.selected == 'selected'){

                        product.count = qty;
                        product.total = qty * product.price;
                        //product.count_replace = true;

                        console.log(product);

                        app.save_to_cart(product);
                    }
                });         
            }
        },

        product_click(id, title, count, price, pvm_group_id){
            
            this.redirect_if_not_logged();

            var product_in_list = {
                'id'    : id,
                'title' : title,
                'count' : count,
                'price' : price,
                'total' : price,
                'pvm_group_id' : pvm_group_id,
                'selected' : '', //class
            }
            
            
            //console.log(this.products_list);
            var product_founded = false;
            
            this.products_list.forEach(function(product, key){
                if (product.id == id){
                    product.count += 1;
                    product.price = parseFloat(product.price);
                    product.total = parseFloat(product.total);
                    product.total += product.price;
                    product.price = product.price.toFixed(2);
                    product.total = product.total.toFixed(2);

                    product_founded = true;
                    product_in_list = product;
                }
            }, id);


            if(!product_founded){
                this.products_list.push(product_in_list);
            }
            

            this.save_to_cart(product_in_list);
        },        

        //------------------ axios --------------------------

    	get_categories(){
            axios.get('get_categories/0').then((res) => {
                this.categories = res.data;
                //console.log(this.categories)
            });    		
    	},

        get_products(){
            axios.get('get_products').then((res) => {
                this.products = res.data;
            });         
        },

        get_cart_products(){
            axios.get('get_cart_products/'+this.user_id).then((res) => {
                this.products_list = res.data;
            });         
        },          

        get_breadcrumb(category_id){

            axios.get('get_breadcrumb/'+category_id).then((res) => {
                this.breadcrumb = res.data;
                this.breadcrumb.reverse();
            }); 
        },  

        category_click(category_id){

            this.redirect_if_not_logged();

            this.get_breadcrumb(category_id);
            axios.get('get_categories/'+category_id).then((res) => {
                this.categories = res.data;
                console.log(this.categories)
            });            

            axios.get('get_products_by_category/'+category_id).then((res) => {
                this.products = res.data;
            }); 
        },
        //----------------------------------------- ------------------             

        go_home(){

            this.redirect_if_not_logged();

            this.get_categories();
            this.get_products();
            this.breadcrumb = [];
        },

        clear_message(timeout) { 

            setTimeout(function(){
                if ($('#payment-modal-id').is(':visible')) {

                    $('#payment-modal-id').modal('toggle');

                    //
                    app.clear_operation();


                    app.message = '';
                    app.money_change2 = 0.00;
                    app.calculator_number = '';
                    app.money_string = '0';
                    app.go_home();
                    app.show_payment_window_full();                    
                }

            }, timeout);
        },

        clear_operation(){
            this.cents_cash = 0;
            this.products_list = [];
        },

        empty_cart(){
            axios.delete('empty_cart').then((res) => {
                
                console.log(res.data);
            });         
        },         

        save_to_cart(product_in_list){
            
            axios.post('save_to_cart', {
                product: product_in_list,
            }).then((res) => {

                console.log(res);
                this.get_cart_products();
                
            })
            .catch(error => {
                //alert('Klaida, bandykite dar kartą');
                console.log(error);
            });       
            
        },        

        save_order() {
            
            //console.log(this.products_list);
            this.money_change2 = this.money_change;

            axios.post('save_orders', {
                products: this.products_list,
                order_title: 'testinis orderis',
                //pvm_rate: this.pvm_rate,
                cash: this.money_cash,
                card: this.money_card,
                money_change: this.money_change
            }).then((res) => {

                console.log(res);
                if(res.status == 200){
                    //OK 

                    this.message = '<div class="alert alert-success">'+
                                        '<strong>Mokėjimas įvykdytas</strong>'+
                                   '</div>';

                    this.products_list = [];
                    this.clear_operation();
                    this.clear_message(5000);
                }
            })
            .catch(error => {
                alert('Klaida, bandykite dar kartą');
                console.log(error);
            });
        }, 

        save_order2() {
            
            //console.log(this.products_list);
            //this.money_change2 = parseFloat(this.money_change_string);

            console.log('money string: '+this.money_string);

            axios.post('save_orders', {
                products: this.products_list,
                order_title: 'testinis orderis',
                //pvm_rate: this.pvm_rate,

                cash: this.money_string,
                card: this.money_card,

                //cash: this.money_card,
                //card: this.money_string,

                money_change: this.money_change_string
            }).then((res) => {

                console.log(res);
                //alert();
                if(res.status == 200){
                    //OK 

                    this.message = '<div class="alert alert-success">'+
                                        '<strong>Mokėjimas įvykdytas</strong>'+
                                   '</div>';

                    //this.money_string = '0';

                    //this.products_list = [];
                    //this.clear_operation();
                    this.show_payment_window_change();

                    this.clear_message(5000);

                    //this.reload_screen();
                }
            })
            .catch(error => {
                alert('Klaida, bandykite dar kartą');
                //console.log(error);
            });
        },   

        save_order_card() {

            //console.log('money string: '+this.money_string);

            axios.post('save_orders', {
                products: this.products_list,
                order_title: 'testinis orderis',
                //pvm_rate: this.pvm_rate,

                cash: 0,
                card: this.money_card,

                money_change: this.money_change_string
            }).then((res) => {

                console.log(res);
                //alert();
                if(res.status == 200){
                    //OK 

                    this.message = '<div class="alert alert-success">'+
                                        '<strong>Mokėjimas įvykdytas</strong>'+
                                   '</div>';

                    this.show_payment_window_change();
                    this.clear_message(5000);
                }
            })
            .catch(error => {
                alert('Klaida, bandykite dar kartą');
                //console.log(error);
            });
        },         

        show_payment_window_change(){
            $('#payment-modal-id .payment-left').addClass('hidden');
            $('#payment-modal-id .price-total').addClass('hidden');
            $('#payment-modal-id .payment-row').addClass('hidden');
        },

        show_payment_window_full(){
            $('#payment-modal-id .payment-left').removeClass('hidden');
            $('#payment-modal-id .price-total').removeClass('hidden');
            $('#payment-modal-id .payment-row').removeClass('hidden');
        },        

        add_calculator_number(nr) {
            this.redirect_if_not_logged();

            var temp_number = this.calculator_number;
            temp_number += nr;
            var validate = !isNaN(temp_number);
            //console.log(validate);

            if(validate)
                this.calculator_number = temp_number;
        },

        delete_calculator_number(){

            this.redirect_if_not_logged();

            this.calculator_number = this.calculator_number.slice(0, -1);
        },



        add_discount_percentage_num(num) {

            this.redirect_if_not_logged();

            //var temp_number = app.calculator_number;
            //if(temp_number.length > 0){
                this.products_list.forEach(function(product_in_list, key){
                    if(product_in_list.selected == 'selected'){ 

                        product_in_list.discount_percentage = num;
                        product_in_list.price_discounted = null;
                        //app.calculator_number = '';

                        //product_in_list.count_replace = true;
                        app.save_to_cart(product_in_list);
                    }
                });                
            //}
        },


        add_discount_percentage() {

            this.redirect_if_not_logged();

            var temp_number = app.calculator_number;
            if(temp_number.length > 0){
                this.products_list.forEach(function(product_in_list, key){
                    if(product_in_list.selected == 'selected'){ 

                        product_in_list.discount_percentage = parseFloat(app.calculator_number);
                        product_in_list.price_discounted = null;
                        app.calculator_number = '';

                        //product_in_list.count_replace = true;
                        app.save_to_cart(product_in_list);
                    }
                });                
            }
        },  
             
        /*
        add_price_discounted() {

            this.redirect_if_not_logged();

            var temp_number = app.calculator_number;
            if(temp_number.length > 0){
                this.products_list.forEach(function(product_in_list, key){
                    if(product_in_list.selected == 'selected'){ 

                        product_in_list.price_discounted = parseFloat(app.calculator_number);
                        product_in_list.discount_percentage = null;
                        app.calculator_number = '';

                        //product_in_list.count_replace = true;
                        app.save_to_cart(product_in_list);
                    }
                });                
            }
        }  
        */      
    
    }
});
