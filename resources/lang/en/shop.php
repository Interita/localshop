<?php

return [

    //index
	'cancel' => 'Reload',
    'customer' => 'Customer',
    'pay' => 'Pay',
    'card_accepted' => 'Card Accepted',
    'search_clean' => 'Clean',
    'products' => 'Products',
    'search_products' => 'Search products',

    //payment
    'back' => 'Back',
    'payment' => 'Payment',
    'cash' => 'Cash',
    'card' => 'Card',
    'gift_coupon' => 'Gift coupon',
    'credit' => 'Credit',
    'receipt' => 'Receipt',
    'receipt_by_mail' => 'Receipt by mail',
    'total_price' => 'Total price',
    'change' => 'CHANGE',

];