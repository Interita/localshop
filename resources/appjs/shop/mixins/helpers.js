
export default {
    mounted() {
        console.log('Helpers Mixin');
    },    
    data: {
        //test_msg: 'test mssssss'
    },
    methods: {

    	
        checkout_window_full(){
            $('#payment-modal-id .payment-left').removeClass('hidden');
            $('#payment-modal-id .price-total').removeClass('hidden');
            $('#payment-modal-id .price-change').removeClass('hidden');

            $('#payment-modal-id .totals-after-payment').addClass('hidden');
            //$('#payment-modal-id .price-total-after').addClass('hidden');
            //$('#payment-modal-id .paid-total-after').addClass('hidden');
            //$('#payment-modal-id .price-change-after').addClass('hidden');            
            $('#payment-modal-id .payment-row').removeClass('hidden');
        }, 

        checkout_window_small(){
            $('#payment-modal-id .payment-left').addClass('hidden');
            $('#payment-modal-id .price-total').addClass('hidden');
            $('#payment-modal-id .price-change').addClass('hidden');

            
            $('#payment-modal-id .totals-after-payment').removeClass('hidden');
            //$('#payment-modal-id .price-total-after').removeClass('hidden');
            //$('#payment-modal-id .paid-total-after').removeClass('hidden');
            //$('#payment-modal-id .price-change-after').removeClass('hidden');
            $('#payment-modal-id .payment-row').addClass('hidden');
        },

        product_discount_filter(product){
            
            //var price = product.price;
            var price = product.price_pvm;

            if(product.price_discounted)
                price = product.price_discounted;

            if(product.discount_percentage){
                var discount = ( parseFloat(product.price_pvm) * parseFloat(product.discount_percentage) ) / 100;
                var new_price = product.price_pvm - discount;
                new_price = new_price.toFixed(2);
                price = new_price;                    
            }

            //console.log('product_discount_filter: ' + price);

            return price;
        },

    }
}