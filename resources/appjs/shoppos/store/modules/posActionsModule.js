const posActionsModule = {

	namespaced: true,

	state: {
		insert_total: 0, 
		withdraw_total: 0,
		refund_total: 0,
		cash_total: 0,
		card_total: 0,
		orders_total: 0,
		money_total: 0,
		money_in_pos: 0,
	},

	getters: {
		insert_total(state){
			return state.insert_total.toFixed(2);
		},
		withdraw_total(state){
			return state.withdraw_total.toFixed(2);
		},
		refund_total(state){
			return state.refund_total.toFixed(2);
		},
		cash_total(state){
			return state.cash_total.toFixed(2);
		},
		card_total(state){
			return state.card_total.toFixed(2);
		},
		orders_total(state){
			return state.orders_total;
		},
		money_total(state){
			return state.money_total.toFixed(2);
		},
		money_in_pos(state){
			return state.money_in_pos.toFixed(2);
		}
	},

	mutations: {
		fetch_actions(state, totals){
            //axios.get('api/pos_actions_get_totals').then((res) => {
                state.insert_total = totals.money_insert_total;
                state.withdraw_total = totals.money_withdraw_total;
                state.refund_total = totals.money_refund_total;
                state.cash_total = totals.cash_total;
                state.card_total = totals.card_total;
                state.orders_total = totals.orders_total;
                state.money_total = totals.money_total;
                state.money_in_pos = totals.money_in_pos;
            //});
		}
	},

	actions: {
		fetch_actions(context){
			return axios.get('api/pos_actions_get_totals').then((res) => {
				context.commit('fetch_actions', res.data);
			});
		}
	}
}


export default posActionsModule