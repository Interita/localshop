<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Product
 * @package App\Models
 * @version November 21, 2017, 12:31 pm UTC
 *
 * @property string title
 * @property string sku
 * @property string barcode
 * @property float price
 * @property integer count
 * @property string image
 */
class Product extends Model
{
    use SoftDeletes;

    //const PVM_PERCENTAGE = 21;

    public $table = 'products';

    public $appends = ['pvm', 'price_pvm', 'price_total'];
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'title',
        'sku',
        'barcode',
        'price',
        'price_purchase',
        'count',
        'image'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'title' => 'string',
        'sku' => 'string',
        'barcode' => 'string',
        'price' => 'float',
        'price_purchase' => 'float',
        'count' => 'integer',
        'image' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'title' => 'required',
        'sku' => 'required',
    ];

    public function categories(){
        return $this->belongsToMany('App\Models\Category');
    }

    public function pvm_group(){
        return $this->belongsTo('App\Models\PvmGroup');
    }

    public function measure(){
        return $this->belongsTo('App\Models\Measure');
    }  


    public function getPvmAttribute(){

        if($this->pvm_group)
            return $this->pvm_group->value;

        return null;

        //$pvm = $this->price * self::PVM_PERCENTAGE / 100;
        ///$price = $this->price + $pvm;
        //return number_format($price, 2);
    }

    public function getPricePvmAttribute(){

        if($this->pvm){
            $pvm = $this->pvm * $this->price / 100;
            $price_pvm = $this->price + $pvm;
            $price_pvm = round($price_pvm, 2);
            return $price_pvm;            
        }

        return null;
    }

    public function getPriceTotalAttribute(){

        $count = isset($this->count) ? $this->count : 0;
        return $this->price * $count;
    }

    public function user_activities()
    {
        //iesko db user_activities, pagal table_id ir table_type
        return $this->morphMany('App\Models\UserActivity', 'table');
    }      

    
}
