<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Supplier
 * @package App\Models
 * @version November 23, 2017, 12:03 pm UTC
 *
 * @property string title
 * @property string address
 * @property string name
 * @property string lastname
 * @property string phone
 * @property string code
 * @property string pvm_code
 */
class Supplier extends Model
{
    use SoftDeletes;

    public $appends = ['products, transactions'];

    public $table = 'suppliers';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'title',
        'address',
        'name',
        'lastname',
        'phone',
        'code',
        'pvm_code'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'title' => 'string',
        'address' => 'string',
        'name' => 'string',
        'lastname' => 'string',
        'phone' => 'string',
        'code' => 'string',
        'pvm_code' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'title' => 'required',
    ];

    public function supplier_documents(){
        return $this->hasMany('App\Models\SupplierDocument');
    }

    public function getProductsAttribute(){
        $products = [];

        foreach ($this->supplier_documents as $document) {
            foreach ($document->transactions as $transaction){
                $products[] = $transaction->product;
            }
        }

        return $products;
    }

    public function getTransactionsAttribute(){
        $transactions = [];

        foreach ($this->supplier_documents as $document) {
            foreach ($document->transactions as $transaction){
                $transactions[] = $transaction;
            }
        }

        return $transactions;
    }    

    
}
