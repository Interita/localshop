<?php

namespace App\Repositories;

use App\Models\Supplier;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class SupplierRepository
 * @package App\Repositories
 * @version November 23, 2017, 12:03 pm UTC
 *
 * @method Supplier findWithoutFail($id, $columns = ['*'])
 * @method Supplier find($id, $columns = ['*'])
 * @method Supplier first($columns = ['*'])
*/
class SupplierRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'title',
        'address',
        'name',
        'lastname',
        'phone',
        'code',
        'pvm_code'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Supplier::class;
    }
}
