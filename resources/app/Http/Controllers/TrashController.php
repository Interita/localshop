<?php

namespace App\Http\Controllers;

use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\Product;

class TrashController extends AppBaseController
{

    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index()
    {
        $categories_count = Category::onlyTrashed()->count();
        $products_count = Product::onlyTrashed()->count();

        $data = [
            'categories_count' => $categories_count,
            'products_count' => $products_count,
        ];
        return view('admin.trash.trash', $data);
    }

}
