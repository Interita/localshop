<?php

use Illuminate\Database\Seeder;

class TypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('types')->insert([
            [
                'name' => 'Pirkimas',
                'created_at' => new \DateTime()
            ],
            [
                'name' => 'Pardavimas',
                'created_at' => new \DateTime()
            ]
        ]);        
    }
}
