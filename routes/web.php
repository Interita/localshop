<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

//--------------------- TESTS -------------------------

Route::get('/test_discounts', 'TestController@test_discounts');
//Route::get('/test_activity', 'TestController@test_activity');
//Route::get('/test_trashed_products', 'TestController@test_trashed_products');
//Route::get('/test_pajam_counts', 'TestController@test_pajam_counts');
//Route::get('/test_order_to_transaction/{order_id}', 'Shop\PointOfSaleController@order_to_transactions');
//-----------------------------------------------------



//------------------ FRONT-END (point of sale) --------------------------
Route::get('/point-of-sale', 'Shop\PointOfSaleController@index')->name('point-of-sale');
Route::get('/point-of-sale-test', 'Shop\PointOfSaleController@index_test')->name('point-of-sale-test');

Route::get('/shop', 'AppJsController@shop_app')->name('shop-app-js');
Route::get('/shop-pos', 'AppJsController@shop_pos_app')->name('shop-pos-app-js');
//-----------------------------------------------------------------------



//----------------- POINT OF SALE API ----------------------------
Route::get('/get_user_id', 'Shop\PointOfSaleController@get_user_id');

Route::get('/get_categories/{parent_id}', 'Shop\PointOfSaleController@get_categories');
Route::get('/get_products', 'Shop\PointOfSaleController@get_products');
Route::get('/get_cart_products/{user_id}', 'Shop\PointOfSaleController@get_cart_products');
Route::get('/get_products_by_category/{category_id}', 'Shop\PointOfSaleController@get_products_by_category');
Route::get('/get_breadcrumb/{category_id}', 'Shop\PointOfSaleController@get_breadcrumb');

//Save Order, Order Items, order status=1, Products--, 
Route::post('/save_order', 'Shop\PointOfSaleController@save_order');

Route::post('/save_to_cart', 'Shop\PointOfSaleController@save_to_cart');
Route::post('/delete_product_from_cart', 'Shop\PointOfSaleController@delete_product_from_cart');
Route::delete('/empty_cart', 'Shop\PointOfSaleController@empty_cart');

Route::get('/get_first_order', 'Shop\PointOfSaleController@get_first_order');
Route::get('/get_first_pos_action', 'Shop\PointOfSaleController@get_first_pos_action');

Route::get('/get_orders', 'Shop\PointOfSaleController@get_orders');
Route::get('/get_order_items/{order_id}', 'Shop\PointOfSaleController@get_order_items');

//------ POS APARATAS ----------
//POS api(order update status=2)
Route::get('/order_generated/{order_id}', 'Shop\PointOfSaleController@order_generated');

//POS api(order update status=3; order_to_transactions)
Route::get('/order_printed/{order_id}/{status_code}', 'Shop\PointOfSaleController@order_printed');

//POS api(order update status=-1)
//Route::get('/order_canceled/{order_id}', 'Shop\PointOfSaleController@order_canceled');
Route::get('/order_canceled/{order_id}/{status_code}', 'Shop\PointOfSaleController@order_canceled');

//---pos actions
Route::post('/pos_action_insert', 'Shop\PointOfSaleController@pos_action_insert');
Route::get('/pos_action_callback', 'Shop\PointOfSaleController@pos_action_callback');

Route::get('/pos_action_change_status_success', 'Shop\PointOfSaleController@pos_action_change_status_success');

Route::get('/pos_action_update_order_status', 'Shop\PointOfSaleController@pos_action_update_order_status');

Route::get('/pos_action_disable_all', 'Shop\PointOfSaleController@pos_action_disable_all');
//------------------------------------------------------------------



//------------------- ADMIN ------------------------------

Route::get('/admin', 'HomeController@index')->name('admin');

//Route::get('/admin/inventory', 'InventoryController@index')->name('inventory.index');
Route::resource('admin/inventory', 'InventoryController');

Route::resource('admin/pajamavimas', 'GoodsReceivingController');
Route::post('/admin-ajax', 'AjaxController@index')->name('admin-ajax'); //AJAX route

Route::resource('admin/products', 'ProductController');
Route::resource('admin/categories', 'CategoryController');
Route::resource('admin/pvmGroups', 'PvmGroupController');
Route::resource('admin/measures', 'MeasureController');

Route::resource('admin/suppliers', 'SupplierController');
Route::resource('admin/supplierDocuments', 'SupplierDocumentController');

Route::resource('admin/roles', 'RoleController');
Route::resource('admin/users', 'UserController');

Route::resource('admin/customers', 'CustomerController');
Route::resource('admin/receipts', 'ReceiptController');

Route::resource('admin/innerDocuments', 'InnerDocumentController');
Route::resource('admin/transactions', 'TransactionController');
Route::resource('admin/types', 'TypeController');

Route::prefix('admin/trash')->group(function(){
	Route::get('/', 'TrashController@index')->name('trash.index');

	//Categories
	Route::get('categories', 'CategoryController@trash')->name('trash.categories');
	Route::patch('categories/{id}', 'CategoryController@restore')->name('trash.categories.restore');

	//Products
	Route::get('products', 'ProductController@trash')->name('trash.products');
	Route::patch('products/{id}', 'ProductController@restore')->name('trash.products.restore');	
});

Route::resource('admin/permissions', 'PermissionController');

Route::get('/admin/reports', 'ReportsController@reports')->name('reports.index');
Route::get('/admin/reports/purchase', 'ReportsController@reports_purchase')->name('reports.purchase');
Route::get('/admin/reports/sale', 'ReportsController@reports_sale')->name('reports.sale');
Route::get('/admin/reports/inventory', 'ReportsController@reports_inventory')->name('reports.inventory');
Route::get('/admin/reports/inventory/{id}', 'ReportsController@reports_inventory_show')->name('reports.inventory.show');

Route::get('/admin/orders', 'OrderController@index')->name('orders.index');
Route::get('/admin/order-items/{order_id}', 'OrderController@order_items')->name('order.items');
//----------------------------------------------------------------------------