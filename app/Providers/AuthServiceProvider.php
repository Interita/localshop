<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        //'App\Model' => 'App\Policies\ModelPolicy',
        //\App\Models\Product::class => \App\Policies\ProductPolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        //
        Gate::define('view-goods-receiving', 'App\Policies\PermissionPolicy@view_goods_receiving');

        Gate::define('view-transactions', 'App\Policies\PermissionPolicy@view_transactions');
        Gate::define('view-types', 'App\Policies\PermissionPolicy@view_types');

        Gate::define('view-products', 'App\Policies\PermissionPolicy@view_products');
        Gate::define('view-categories', 'App\Policies\PermissionPolicy@view_categories');
        Gate::define('view-pvm-groups', 'App\Policies\PermissionPolicy@view_pvm_groups');
        Gate::define('view-measures', 'App\Policies\PermissionPolicy@view_measures');

        Gate::define('view-suppliers', 'App\Policies\PermissionPolicy@view_suppliers');
        Gate::define('view-supplier-documents', 'App\Policies\PermissionPolicy@view_supplier_documents');

        Gate::define('view-customers', 'App\Policies\PermissionPolicy@view_customers');
        Gate::define('view-receipts', 'App\Policies\PermissionPolicy@view_receipts');
        Gate::define('view-orders', 'App\Policies\PermissionPolicy@view_orders');

        Gate::define('view-inner-documents', 'App\Policies\PermissionPolicy@view_inner_documents');

        Gate::define('view-users', 'App\Policies\PermissionPolicy@view_users');
        Gate::define('view-roles', 'App\Policies\PermissionPolicy@view_roles');
        Gate::define('view-permissions', 'App\Policies\PermissionPolicy@view_permissions');

        Gate::define('view-reports', 'App\Policies\PermissionPolicy@view_reports');
        Gate::define('view-inventory', 'App\Policies\PermissionPolicy@view_inventory');
    }
}
