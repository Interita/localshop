<?php

namespace App\Repositories;

use App\Models\Permission;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class PermissionsRepository
 * @package App\Repositories
 * @version December 12, 2017, 7:51 am UTC
 *
 * @method Permissions findWithoutFail($id, $columns = ['*'])
 * @method Permissions find($id, $columns = ['*'])
 * @method Permissions first($columns = ['*'])
*/
class PermissionRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'title',
        'name'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Permission::class;
    }
}
