<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Customer
 * @package App\Models
 * @version November 24, 2017, 9:41 am UTC
 *
 * @property string name
 * @property string lastname
 * @property string address
 * @property string image
 */
class Customer extends Model
{
    use SoftDeletes;

    public $table = 'customers';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'name',
        'lastname',
        'company_name',
        'company_nr',
        'address',
        'vat',
        'image' 
    ];



    /**
     * .
     *
     * @var array
     */
    protected $casts = [
        'name' => 'string',
        'lastname' => 'string',
        'company_name' => 'string',
        'company_nr' => 'string',
        'address' => 'string',
        'vat' => 'string',
        'image' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required',
    ];

    
}
