<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InventoryActivitiesItem extends Model
{
	public $appends = ['shortage', 'shortage_total'];

    public function product(){
    	return $this->belongsTo('App\Models\Product');
    }

    public function getShortageAttribute(){
    	
    	return $this->count - $this->count_real;
    }

    //trukstamo kiekio pinigu suma
    public function getShortageTotalAttribute(){ 
    	
    	return $this->shortage * $this->price;
    }    
}
