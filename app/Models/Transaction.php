<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Transaction
 * @package App\Models
 * @version November 24, 2017, 10:20 am UTC
 *
 * @property integer product_id
 * @property integer document_id
 * @property integer type_id
 * @property integer user_id
 * @property integer count
 * @property float price
 * @property float original_price
 */
class Transaction extends Model
{
    use SoftDeletes;

    public $table = 'transactions';
    //public $appends = [];

    protected $dates = ['deleted_at'];


    public $fillable = [
        'product_id',
        'document_id',
        'type_id',
        'user_id',
        'count',
        'price',
        'original_price'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'product_id' => 'integer',
        'document_id' => 'integer',
        'type_id' => 'integer',
        'user_id' => 'integer',
        'count' => 'integer',
        'price' => 'float',
        'original_price' => 'float'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    public function product(){
        return $this->belongsTo('App\Models\Product');
    }

    public function type(){
        return $this->hasOne('App\Models\Type', 'id', 'type_id');
    }

    public function user(){
        return $this->hasOne('App\Models\User', 'id', 'user_id');
    }



    public function document(){
        return $this->morphTo();
    }

    //--- finds existing or deleted product title
    public function getProductTitle(){

        $product_title = false;

        if ($this->product){
            $product_title = $this->product->title;
        }
        else{
            $trashed_product = get_trashed_object($this->product_id, Product::class);
            if($trashed_product){
                $product_title = $trashed_product->title;
            }
        } 

        return $product_title;       
    }

    public function scopeDatesFilter($query, $dates){
        
        $has_dates = false;

        foreach ($dates as $date) {
            if(isset($date))
                $has_dates = true;
            else
                $has_dates = false;
        }

        if($has_dates)
            return $query->whereBetween('created_at', $dates);

        return $query;
    }


    public function scopeTypesFilter($query, $types){

        if(isset($types))
            return $query->whereIn('type_id', $types);

        return $query;
    }
    
}
