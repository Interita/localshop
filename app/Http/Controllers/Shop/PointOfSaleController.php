<?php

namespace App\Http\Controllers\Shop;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\DB;
use App\Models\Transaction;
use App\Models\Category;
use App\Models\Product;
use App\Models\Order;
use App\Models\OrderItem;
use App\Models\Cart;
use App\Models\Discount;
use App\Models\PosAction;
use Auth;

define("PARDAVIMAS", 2);

class PointOfSaleController extends Controller
{



    public function __construct()
    {
        //$this->middleware('auth');

        //$this->middleware('can:view-point-of-sale');
    }

    public function index(){
    	$products = Product::all();

    	$data = [
    		'products' => $products,
    	];

    	return view('shop.index', $data);
    }

    public function index_test(){ //point-of-sale-test

        return view('test');
    }    

    public function get_user_id(){
        $user_id = Auth::id();
        if(!isset($user_id))
            return 0;
        
        return $user_id;
    }

    public function get_first_order(){

        $order = Order::where('status', 1)->get(['id', 'status', 'cash', 'card'])->first();

        if($order){
			$order_items = OrderItem::where('order_id', $order->id)->get(['id', 'title', 'count', 'price', 'price_pvm', 'extra_text', 'pvm_group_id',]);

            $order_items_formated = [];
            foreach ($order_items as $item) {
                $order_items_formated[] = [
                    'id' => $item->id,
                    'title' => $item->title,
                    'count' => $item->count,
                    'price' => $item->price,
                    'price_pvm' => $item->price_pvm,
                    'extra_text' => $item->extra_text,
                    'pos_pvm_option' => $item->pvm_group_id,
                ]; 
            }

			$data = [
				'order_id'  => $order->id,
				'cash'  => $order->cash,
				'card'  => $order->card,
				'status'	=> $order->status,
				//'items' => $order_items,
                'items' => $order_items_formated,
			];
        }
		else{
			$data = [
				'status' => -1
			];
		}


        return response()->json($data, 200, [], JSON_UNESCAPED_UNICODE);   	
    }  

    public function get_first_pos_action(){

        $pos_actions = PosAction::where('status', 1)->get(['id', 'action', 'insert', 'withdraw'])->first();
        return response()->json($pos_actions, 200, [], JSON_UNESCAPED_UNICODE);      
    }  

    public function get_orders(){

        $orders = Order::where('status', 1)->get(['id', 'status']);
        return response()->json($orders, 200, [], JSON_UNESCAPED_UNICODE);   	
    }  

    public function get_order_items($order_id){

        $order_items = OrderItem::where('order_id', $order_id)->get(['id', 'title', 'count', 'price']);
        return response()->json($order_items, 200, [], JSON_UNESCAPED_UNICODE);   	
    } 

    public function order_generated($order_id){
    	$order = Order::find($order_id);
    	$order->status = 2;
    	$saved = $order->save();

    	$data = [
    		'status' => 2,
    		'order' => $order_id,
    		'saved' => $saved
    	];
    	echo json_encode($data);
    }   


    public function save_order(Request $request){

        $order = new Order;
        $status = 0;
        $products = $request->products;
        $user_id = Auth::id();
        $new_price = 0;

        
        if(!empty($products)){
            $status = 1;

            $order->title = $request->order_title;
            $order->cash = $request->cash;
            $order->card = $request->card;   
            $order->money_change = $request->money_change;
            
            $order->status = 1; //PRADINIS STATUS

            $order->user_id = $user_id;

            $discount_percentage = -1;

            DB::transaction(function() use ($order, $products, $discount_percentage) {
                $order->save();

                foreach ($products as $product) {
                    

                    $price = isset($product['price']) ? $product['price'] : 0;
                    $price_pvm = isset($product['price_pvm']) ? $product['price_pvm'] : 0;
                    $discount_percentage = isset($product['discount_percentage']) ? $product['discount_percentage'] : null;
                    $price_discounted = isset($product['price_discounted']) ? $product['price_discounted'] : null;

                    //su nuolaida
                    $new_price_pvm = $this->get_discounted_price($price_pvm, $price_discounted, $discount_percentage);

                    //su pvm
                    //$price_pvm = get_price_pvm($new_price);
                    //$price_pvm = $product['price_pvm'];

                    
                    $order_item = new OrderItem;
                    $order_item->order_id = $order->id;
                    $order_item->product_id = $product['id'];
                    $order_item->title = $product['title'];
                    $order_item->count = $product['count'];
                    //$order_item->price = $product['price'];
                    $order_item->price = $price;
                    $order_item->price_pvm = $new_price_pvm;
                    if(isset($discount_percentage)){
                        $order_item->extra_text = $price_pvm.' - '.$discount_percentage.'%';

                        //$discount = new Discount;
                        //$discount->discount_percentage = $discount_percentage;
                        //$order_item->discounts()->save($discount); 
                    }
                    $order_item->pvm_group_id = $product['pvm_group_id'];
                    $order_item->save(); 
                    

                    //PRODUCT--
                    $product_db = Product::find($product['id']);
                    $product_db->count -= $product['count'];
                    $product_db->save();
                }

                //DELETE USER CART
                $this->empty_cart();

                //POS_ACTIONS
                $this->save_order_pos_actions($order->id);

                //-------- TESTUOJAM (po cekio isspausdinimo sita func paleidziama) ----------
                //$transaction_ids = $this->order_to_transactions($order->id, $order->user_id);

                //----------------------------------------------------------------------------

            });  
             
            $status = 2;
        }
        
        
        $data = [
            'status' => $status,
            'discount_percentage' => isset($discount_percentage) ? $discount_percentage : -3,
            //'new_price' => $new_price,
            'order_id' => $order->id,
            'products' => $request->products,
        ];

        echo json_encode($data);
        
    }   

    private function save_order_pos_actions($order_id){

        //$order_id = 1;
        $pos_action = new PosAction;
        $pos_action->action = 'ORDER_ID';
        $pos_action->value = $order_id;
        $pos_action->status = 'PROCESSING';
        $res = $pos_action->save();        
    }


    private function get_discounted_price($original_price, $price_discounted, $discount_percentage){
        
        $price = $original_price;

        if($price_discounted){
            $price = $price_discounted;
        }
        else{
            if($discount_percentage){
                $discount = ( $price * $discount_percentage ) / 100;
                $new_price = $price - $discount;
                $new_price = round($new_price, 2);
                $price = $new_price;                    
            }            
        }

        return $price;      
    }  

    public function order_canceled(Request $request){
        //order_id
        //res
        $order_id = $request->order_id;
        $status_code = $request->status_code;

        $status = -1;

        //----- pos actions status -------
        $post_actions = PosAction::where('action', 'ORDER_ID')
            ->where('value', $order_id)
            //->where('status', 'PROCESSING')
            ->first();
        if($post_actions){
            $post_actions->status = 'ERROR';
            $post_actions->message = $status_code;
            $post_actions->update();
        }
        else{
            $status = -3;
        }
        //--------------------------   


        //------- order status -------------
        $order = Order::find($order_id);
        if($order){
            $order->status = $status;
            $saved = $order->save();            
        }
        else{
            $status = -2;
        }
        //-----------------------------------


        $data = [
            'status' => $status,
            'order' => $order_id,
        ];
        echo json_encode($data);
    }     

    //api kvieciamas per POS SERVICE (test version)
    public function order_printed(Request $request){

        $order_id = $request->order_id;
        $status_code = $request->status_code;
        $status = 3;
        $saved = 0;


        //----- pos actions status -------
        $post_actions = PosAction::where('action', 'ORDER_ID')
            ->where('value', $order_id)
            ->where('status', 'PROCESSING')->first();

        if($post_actions){
            $post_actions->status = 'OKPOS';
            $post_actions->message = $status_code;
            $post_actions->update();
        }
        else{
            $status = -5;
        }
        //--------------------------

        //------- order status -------
        $order = Order::find($order_id);
        if($order){
            $order->status = $status;
            $saved = $order->save();
        }
        else{
            $status = -4;
        }
        //-----------------------------        

        $transaction_ids = 0;
        if($status === 3){
            //------ add transaction -------------
            $user_id = $order->user_id; //?
            $transaction_ids = $this->order_to_transactions($order_id, $user_id);
            //-------------------------------------  
        }
      

    	$data = [
    		'status' => $status,
    		'order' => $order_id,
    		'transaction ids' => $transaction_ids,
    		'saved' => $saved
    	];
    	echo json_encode($data);
    }

    //
    public function order_to_transactions($order_id, $user_id){

    	$transaction_ids = [];

        $order = Order::find($order_id);
        
        if($order){
            $order_items = OrderItem::where('order_id', $order_id)->get();

            foreach ($order_items as $item){
                $type_id = PARDAVIMAS;
                $transaction_ids[] = $this->insert_transaction(
                    $order, 
                    $type_id, 
                    $item->product_id, 
                    $item->count, 
                    null, 
                    $item->price,
                    $user_id
                );      
            }
        }

    	return $transaction_ids;
    }

    //Pardavimas
    private function insert_transaction($order, $type_id, $product_id, $count, $price_purchase, $price_sale, $user_id){

        //$order = Order::find($document_id);

        if($order){
            $transaction = new Transaction;
            //$transaction->document_id = $document_id;
            //$transaction->document_type = 'App\Models\Order'; //pardavimas
            $transaction->type_id = $type_id;
            $transaction->product_id = $product_id;
            $transaction->count = $count;
            $transaction->price_purchase = $price_purchase;
            $transaction->price = $price_sale;
            $transaction->user_id = $user_id;
            //$transaction->save();

            $order->transactions()->save($transaction);

            return $transaction->id;            
        }

        return false;
    }


    public function get_categories($parent_id){

        $categories = Category::all()->where('parent_id', $parent_id);
        return response()->json($categories, 200, [], JSON_UNESCAPED_UNICODE);
    }    

    public function get_products(){

        $products = Product::all(
            ['id', 'title', 'sku', 'barcode', 'price', 'image', 'pvm_group_id', 'measure_id']
        );
        return response()->json($products, 200, [], JSON_UNESCAPED_UNICODE);
    }

    public function get_products_by_category($category_id){

        $category = Category::find($category_id);

        $products = $category->products;
        return response()->json($products, 200, [], JSON_UNESCAPED_UNICODE);
    }

    public function get_breadcrumb($category_id){
        $breadcrumb = [];
        $category = Category::find($category_id);
        while(!empty($category)){
            $breadcrumb[] = $category;
            $category = Category::find($category->parent_id);
        }

        return response()->json($breadcrumb, 200, [], JSON_UNESCAPED_UNICODE);        
    }    

    public function empty_cart(){
        $user_id = Auth::id();

        $cart = Cart::where('user_id', $user_id)->get();
        foreach ($cart as $product) {
            $product->discounts()->delete();
        }

        $cart = Cart::where('user_id', $user_id)->delete();  

        //echo 'empty count: '.$cart;  	
    }

    public function get_cart_products($user_id){

    	$products_arr = [];
        $products = Cart::where('user_id', $user_id)->get();

        foreach ($products as $product) {

            //total skaiciuojamas pritaikius nuolaidas (front-end vue)
        	//$total = $product->price * $product->count;

            $discount = $product->discounts->last();
            $price_discounted = $discount ? $discount->price_discounted : null;
            $discount_percentage = $discount ? $discount->discount_percentage : null;

        	$products_arr[] = [
        		'id' => $product->product_id,
        		'title' => $product->title,
        		'count' => $product->count,
        		//'price' => number_format($product->price, 2),
        		'price' => $product->price,
                'price_pvm' => $product->price_pvm,
                //'price_discounted' => $product->price_discounted,
                //'discount_percentage' => $product->discount_percentage,
                'price_discounted' => $price_discounted,
                'discount_percentage' => $discount_percentage,
        		//'total' => $total,
        		'pvm_group_id' => $product->pvm_group_id,
        		'selected' => '',
        	];
        }

        return response()->json($products_arr, 200, [], JSON_UNESCAPED_UNICODE);
    }        

    public function save_to_cart(Request $request){
    	
    	$res = 0;
    	$user_id = Auth::id();
    	$product_id = $request->product['id'];

    	$cart = Cart::where('product_id', $product_id)->where('user_id', $user_id)->first();


        if($cart){ //UPDATE PRODUCT

            //------------- discounts -------------
            if( isset($request->product['price_discounted']) || isset($request->product['discount_percentage']) ){
                
                $update_discount = $cart->discounts->last();
                if($update_discount){
                    if( isset($request->product['price_discounted']) )
                        $update_discount->discount_percentage = $request->product['price_discounted'];
                    if( isset($request->product['discount_percentage']) )
                        $update_discount->discount_percentage = $request->product['discount_percentage'];

                    $update_discount->update();                    
                }
                else{
                    $discount = new Discount;
                    if( isset($request->product['price_discounted']) )
                        $discount->discount_percentage = $request->product['price_discounted'];
                    if( isset($request->product['discount_percentage']) )
                        $discount->discount_percentage = $request->product['discount_percentage'];    
                    
                    $cart->discounts()->save($discount);                
                }
            }
            //----------------------------------------------
            
			$cart->count = $request->product['count']; //update count
    		$res = $cart->update();
    	}
    	else{ //SAVE PRODUCT
	    	$cart = new Cart;
	    	$cart->user_id = $user_id;
	    	$cart->product_id = $product_id;
	    	$cart->title = $request->product['title'];
	    	$cart->count = $request->product['count'];
	    	$cart->price = $request->product['price'];
	    	$cart->pvm_group_id = $request->product['pvm_group_id'];
	    	$res = $cart->save();    		
    	}

    	echo $res;
    }

    public function delete_product_from_cart(Request $request){
        
        $res = 0;
        $user_id = Auth::id();
        $product_id = $request->product['id'];

        $cart = Cart::where('product_id', $product_id)->where('user_id', $user_id)->first();

        if($cart){ //DELETE PRODUCT FROM CART

            $res = $cart->delete();
        }

        echo $res;
        
    }    


    //------------------- SHOP POS --------------
    public function pos_action_insert(Request $request){

        $pos_action = new PosAction;
        $pos_action->action = $request->action;
        $pos_action->value = $request->value;
        $pos_action->message = isset($request->message) ? $request->message : null;
        $pos_action->status = 'PROCESSING';
        $res = $pos_action->save();
        
        $data = [
            'id' => $pos_action->id,
            'status' => $pos_action->status,
            'action' => $pos_action->action
        ];
        echo json_encode($data); 
                    
    }

    public function pos_action_callback(Request $request){ 

        $pos_action = new PosAction;
        $pos_status = isset($request->status) ? $request->status : null;        
        $res = $pos_action::where('status', $pos_status)->first();

        if ($res){
            $data = [
                'id' => $res->id,
                //'send_status' => $pos_status,
                
                'action' => $res->action,
                'value' => $res->value,
                'status' => $res->status,
                //'insert' => $res->insert,
                //'withdraw' => $res->withdraw,
                //'refund' => $res->refund,
                'message' => $res->message,
            ];
            //echo json_encode($data);     
            return response()->json($data, 200, [], JSON_UNESCAPED_UNICODE);       
        }
        else{
            $data = [
                'waiting_for' => $pos_status
            ];
            //echo json_encode($data);            
            return response()->json($data, 200, [], JSON_UNESCAPED_UNICODE);
        }

    }

    public function pos_action_change_status_success(Request $request){ 

        //POS SISTEMA ir SHOP KVIECIA si API

        //$request->id = 1;
        $pos_status_check = isset($request->status_check) ? $request->status_check : null;
        $pos_status_change = isset($request->status_change) ? $request->status_change : null;
        //$pos_action = PosAction::where('status', $pos_status_check)->first();
        $pos_action = PosAction::where([
            ['action', '<>', 'ORDER_ID'],
            ['status', '=', $pos_status_check],
        ])->first();        
        
        $res = false;
        if($pos_action){
            $pos_action->status = $pos_status_change;
            $res = $pos_action->save();
        }
        

        if ($res){
            $data = [
                'id' => $pos_action->id,
                'status' => $pos_action->status,
                'action' => $pos_action->action,
            ];
            //echo json_encode($data); 
            return response()->json($data, 200, [], JSON_UNESCAPED_UNICODE);            
        }
        else{
            $data = [
                'status' => "0",
                //'request' => $request
            ];
            //echo json_encode($data);   
            return response()->json($data, 200, [], JSON_UNESCAPED_UNICODE);         
        }

    }    


    public function pos_action_update_order_status(Request $request){ 
        /*
            find pos_action by: 
              order_id
              status(status_check)
            update pos_action status(status_change)
        */

        $pos_status_check = isset($request->status_check) ? $request->status_check : null;
        $pos_status_change = isset($request->status_change) ? $request->status_change : null;
        $pos_action = PosAction::where([
            ['action', '=', 'ORDER_ID'],
            ['status', '=', $pos_status_check],
        ])->first();
        
        
        $res = false;
        if($pos_action){
            $pos_action->status = $pos_status_change;
            $res = $pos_action->save();
        }
        

        if ($res){
            $data = [
                'id' => $pos_action->id,
                'status' => $pos_action->status,
                'action' => $pos_action->action,
                'order_id' => $pos_action->value,
            ];
            //echo json_encode($data); 
            return response()->json($data, 200, [], JSON_UNESCAPED_UNICODE);            
        }
        else{
            $data = [
                'status' => "0",
                //'request' => $request
            ];
            //echo json_encode($data);   
            return response()->json($data, 200, [], JSON_UNESCAPED_UNICODE);         
        }

    }      
    //--------------------------------------------------



    public function pos_action_disable_all(){
        $disabled = PosAction::where('disabled', false)->update(['disabled' => true]);
        $data = [
            'disabled' => $disabled,
        ]; 
        return response()->json($data, 200, [], JSON_UNESCAPED_UNICODE);
    }

    public function pos_actions_get_totals(){

        //------- MONEY INSERT -------------
        $pos_actions = PosAction::where([
            ['action', 'MONEY_INSERT'],
            ['status', 'OK'],
            ['disabled', false],
        ])->get();
        $money_insert_total = 0;
        foreach ($pos_actions as $action) {
            $money_insert_total += isset($action->value) ? $action->value : 0;
        }
        //-----------------------------------

        //-------- MONEY WITHDRAW -----------
        $pos_actions = PosAction::where([
            ['action', 'MONEY_WITHDRAW'],
            ['status', 'OK'],
            ['disabled', false],
        ])->get();
        $money_withdraw_total = 0;
        foreach ($pos_actions as $action) {
            $money_withdraw_total += isset($action->value) ? $action->value : 0;
        }
        //-----------------------------------

        //-------- MONEY REFUND -------------
        $pos_actions = PosAction::where([
                ['action', 'MONEY_REFUND'],
                ['status', 'OK'],
                ['disabled', false],
        ])->get();

        $money_refund_total = 0;
        foreach ($pos_actions as $action) {
            $money_refund_total += isset($action->value) ? $action->value : 0;
        }
        //-------------------------------------    

        $pos_actions = PosAction::where([
                ['action', 'ORDER_ID'],
                ['status', 'OK'],
                ['disabled', false],
        ])->get();
        $orders_str = '';
        $cash_total = 0;
        $card_total = 0;
        $credit_total = 0;
        $money_total = 0;
        foreach ($pos_actions as $action) {
            $order_id = isset($action->value) ? $action->value : 0;
            $order = Order::find($order_id);
            if($order){
                $orders_str .= $order->id . '; ';
                $action_cash_total = $order->cash - $order->money_change;
                $cash_total += $action_cash_total;
                $card_total += $order->card;
                //$credit_total += $order->credit;

                $money_total += $action_cash_total + $order->card;
            }
        }  
        $orders_total = count($pos_actions);   

        //-refunds
        //$cash_total -= $money_refund_total;
        //$money_total -= $money_refund_total;

        //----- grynuju stalciuje ------
        $money_in_pos = 0;
        $money_in_pos += $money_insert_total;
        $money_in_pos -= $money_withdraw_total;
        $money_in_pos -= $money_refund_total;
        $money_in_pos += $cash_total;
        //------------------------------

        $data = [
            'money_insert_total' => $money_insert_total,
            'money_withdraw_total' => $money_withdraw_total,
            'money_refund_total' => $money_refund_total,
            'cash_total' => $cash_total,
            'card_total' => $card_total,
            'credit_total' => $credit_total,
            //'orders' => $orders_str,
            'orders_total' => $orders_total,
            'money_total' => $money_total,
            'money_in_pos' => $money_in_pos,
        ]; 
        return response()->json($data, 200, [], JSON_UNESCAPED_UNICODE);        
    }    

}
