<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AppJsController extends Controller
{

    public function shop_app(){
        
        return view('appjs/shopapp');
    }

    public function shop_pos_app(){
        
        return view('appjs/shopposapp');
    }      

}
