let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js')
   .sass('resources/assets/sass/app.scss', 'public/css');


//mix.js('resources/assets/js/testapp.js', 'public/js');

//mix.js('resources/assets/js/point_of_sale_app.js', 'public/js');

//mix.js('appjs/shop/shop.js', 'public/js');
mix.js('appjs/shopApp.js', 'public/js');
mix.js('appjs/shopPosApp.js', 'public/js');