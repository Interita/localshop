import Vue from 'vue'
import Vuex from 'vuex'
import posActionsModule from './modules/posActionsModule'

Vue.use(Vuex)

export default new Vuex.Store({

	modules: {
		posActions: posActionsModule,
	},

})